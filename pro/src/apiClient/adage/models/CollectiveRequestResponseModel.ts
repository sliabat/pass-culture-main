/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type CollectiveRequestResponseModel = {
  comment: string;
  email: string;
  phoneNumber?: string | null;
  requestedDate?: string | null;
  totalStudents?: number | null;
  totalTeachers?: number | null;
};

