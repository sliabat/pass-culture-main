/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type GetOfferManagingOffererResponseModel = {
  id: string;
  name: string;
  nonHumanizedId: number;
};

