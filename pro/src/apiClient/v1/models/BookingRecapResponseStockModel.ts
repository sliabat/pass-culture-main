/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type BookingRecapResponseStockModel = {
  eventBeginningDatetime?: string | null;
  offerId: number;
  offerIdentifier: string;
  offerIsEducational: boolean;
  offerIsbn?: string | null;
  offerName: string;
  stockIdentifier: string;
};

