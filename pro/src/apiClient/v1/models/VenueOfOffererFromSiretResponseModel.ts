/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type VenueOfOffererFromSiretResponseModel = {
  id: string;
  isPermanent: boolean;
  name: string;
  publicName?: string | null;
  siret?: string | null;
};

