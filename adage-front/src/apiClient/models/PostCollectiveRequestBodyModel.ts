/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type PostCollectiveRequestBodyModel = {
  comment: string;
  phoneNumber?: string | null;
  requestedDate?: string | null;
  totalStudents?: number | null;
  totalTeachers?: number | null;
};

